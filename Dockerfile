FROM python:3.11.3


WORKDIR /src

COPY src/ .
COPY . .

RUN pip install poetry
RUN poetry install