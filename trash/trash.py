def make_url(path):
    base = 'https://openseller.ru/'.rstrip('/')
    path = path.lstrip("/")
    return "/".join((base, path))


print(make_url('/auth/users/'))
print(make_url('auth/users'))
