from fastapi import APIRouter

from core.config import config
from .schemas import Health

router = APIRouter(tags=["Мониторинг"])


@router.get("/health/")
async def health() -> Health:
    return Health(version=config.RELEASE_VERSION, status="Healthy")
