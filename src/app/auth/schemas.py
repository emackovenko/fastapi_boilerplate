import re
from typing import Any

from pydantic import BaseModel, EmailStr, constr, validator, Field

from .models import User


class Token(BaseModel):
    access_token: str = Field(..., description='Токен доступа')
    refresh_token: str = Field(..., description='Токен обновления')


class AbstractUserResponse(BaseModel):
    """
    Абстрактный класс пользователя
    """

    id: int = Field(None, description='Идентификатор', example='1234')
    email: str | None = Field(..., description='Электронная почта', example='john.doe@example.com')

    class Config:
        orm_mode = True


class UserResponse(AbstractUserResponse):
    """
    Схема пользователя
    """
    pass


class CurrentUser(BaseModel):
    id: int | None = Field(None, description="ID пользователя")

    class Config:
        validate_assignment = True


class CurrentUserResponse(UserResponse):
    """
    Схема текущего пользователя
    """
    token: Token | None = Field(default=None, description='Токен авторизации')

    @classmethod
    def from_user_token_pair(cls, user: User, token: Token) -> Any:
        """
        Возвращает экземпляр объекта из пары 'Пользователь-Токен'
        """
        result = CurrentUserResponse.from_orm(user)
        result.token = token
        return result


class AbstractUserRequest(BaseModel):
    """
    Абстрактный класс для редактиремого пользователя
    """

    email: EmailStr


class RegisterUserRequest(AbstractUserRequest):
    """
    Регистрация пользователя
    """

    password: constr(min_length=8, max_length=64)

    @validator('password')
    def password_must_contain_special_characters(cls, v):
        if not re.search(r'[^a-zA-Z0-9]', v):
            raise ValueError('Пароль должен содержать спец. символы')
        return v

    @validator('password')
    def password_must_contain_numbers(cls, v):
        if not re.search(r'[0-9]', v):
            raise ValueError('Пароль должен содержать цифры')
        return v

    @validator('password')
    def password_must_contain_uppercase(cls, v):
        if not re.search(r'[A-Z]', v):
            raise ValueError('Пароль должен содержать большие буквы')
        return v

    @validator('password')
    def password_must_contain_lowercase(cls, v):
        if not re.search(r'[a-z]', v):
            raise ValueError('Пароль должен содержать маленькие буквы')
        return v


class LoginUserRequest(BaseModel):
    email: EmailStr
    password: str
