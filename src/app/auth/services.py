from functools import partial
from typing import Tuple

from fastapi import Depends

from core.database import get_session
from core.exceptions import BadRequestException, UnauthorizedException
from core.security import JWTHandler, PasswordHandler
from core.service import BaseService
from .models import User
from .repositories import UserRepository
from .schemas import RegisterUserRequest, LoginUserRequest, Token, AbstractUserRequest


class AuthFactory:
    """
    This is the factory container that will instantiate all the services and
    repositories which can be accessed by the rest of the application.
    """

    # Repositories
    user_repository = partial(UserRepository, User)

    def get_user_service(self, db_session=Depends(get_session)):
        return UserService(
            user_repository=self.user_repository(db_session=db_session)
        )


class UserService(BaseService[User]):
    def __init__(
        self,
        user_repository: UserRepository
    ):
        super().__init__(model=User, repository=user_repository)
        self.user_repository = user_repository

    def __generate_new_user_token(self, user: User) -> Token:
        """
        Вовзращает новый токен пользователя
        """
        payload = {
            'user_id': user.id,
        }
        return Token(
            access_token=JWTHandler.encode(payload=payload),
            refresh_token=JWTHandler.encode(payload={'sub': 'refresh_token'}),
        )

    async def __create_user(self, data: AbstractUserRequest, **kwargs) -> User:
        """
        Создает пользователя в базе данных
        """
        user: User | None = None

        if data.email:
            user = await self.user_repository.get_by_email(data.email)
        if user:
            raise BadRequestException(
                'Пользователь с такими данными уже существует.'
            )

        password = PasswordHandler.hash(kwargs.get('password'))

        return await self.user_repository.create(
            email=data.email,
            password=password,
        )

    async def register(self, data: RegisterUserRequest) -> Tuple[User, Token]:

        # создаем пользователя
        user = await self.__create_user(
            data,
            password=data.password,
        )
        return await self.login(LoginUserRequest(email=user.email,
                                                 password=data.password))

    async def login(self, data: LoginUserRequest) -> Tuple[User, Token]:
        user = await self.user_repository.get_by_email(data.email)

        if not user:
            raise BadRequestException('Пользователь не найден.')

        if not PasswordHandler.verify(user.password, data.password):
            raise BadRequestException('Пользователь не найден.')

        token = self.__generate_new_user_token(user)
        return user, token

    async def refresh_token(self, access_token: str, refresh_token: str) -> Token:
        token = JWTHandler.decode(access_token)
        refresh_token = JWTHandler.decode(refresh_token)
        if refresh_token.get('sub') != 'refresh_token':
            raise UnauthorizedException('Invalid refresh token')

        return self.__generate_new_user_token(token.get('user_id'))
