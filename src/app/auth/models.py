from sqlalchemy import BigInteger, Column, Unicode

from app import Base
from core.database.mixins import TimestampMixin
from core.orm.models import AbstractModel


class User(Base, TimestampMixin, AbstractModel):
    """
    Пользователь системы
    """
    __tablename__ = 'users'

    id = Column(BigInteger, primary_key=True, autoincrement=True, comment='Идентификатор')
    email = Column(Unicode(255), nullable=True, unique=True, comment='e-mail')
    password = Column(Unicode(255), nullable=False, comment='Пароль (хэш)')

    __table_args__ = {'extend_existing': True}
    __mapper_args__ = {'eager_defaults': True}
