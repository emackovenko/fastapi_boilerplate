from fastapi import APIRouter, Depends

from .dependencies import AuthenticationRequired, current_user
from .models import User
from .schemas import LoginUserRequest, RegisterUserRequest, UserResponse, CurrentUserResponse
from .services import UserService, AuthFactory

router = APIRouter(tags=['Users'])


@router.get('/users')
async def get_users(
    user_service: UserService = Depends(AuthFactory().get_user_service),
) -> list[UserResponse]:
    users = await user_service.get_all()
    return users


@router.post('/signup', status_code=201, description='Регистрация пользователя')
async def register_user(
    request: RegisterUserRequest,
    user_service: UserService = Depends(AuthFactory().get_user_service),
) -> CurrentUserResponse:
    user, token = await user_service.register(request)
    return CurrentUserResponse.from_user_token_pair(user, token)


@router.post('/signin', description='Авторизация пользователя')
async def login_user(
    request: LoginUserRequest,
    user_service: UserService = Depends(AuthFactory().get_user_service),
) -> CurrentUserResponse:
    user, token = await user_service.login(request)
    return CurrentUserResponse.from_user_token_pair(user, token)


@router.get('/me', dependencies=[Depends(AuthenticationRequired)])
def get_user(
    user: User = Depends(current_user),
) -> UserResponse:
    return user
