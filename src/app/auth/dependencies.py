from fastapi import Depends, Request
from fastapi.security import HTTPAuthorizationCredentials, HTTPBearer

from .exceptions import AuthenticationRequiredException
from .services import UserService, AuthFactory


async def current_user(
    request: Request,
    user_service: UserService = Depends(AuthFactory().get_user_service),
):
    return await user_service.get_by_id(request.user.id)


class AuthenticationRequired:
    def __init__(
        self,
        token: HTTPAuthorizationCredentials = Depends(HTTPBearer(auto_error=False)),
    ):
        if not token:
            raise AuthenticationRequiredException()
