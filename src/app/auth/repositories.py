from core.repository import BaseRepository
from .models import User


class UserRepository(BaseRepository[User]):
    """
    Репозиторий пользователя.
    """

    async def get_by_email(
        self, email: str
    ) -> User | None:
        """
        Возвращает пользователя по электронной почте.
        """
        return await self.get(
            email__iexact=email, limit=1
        )
