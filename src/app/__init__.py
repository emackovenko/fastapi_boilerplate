from fastapi import APIRouter

from core.database import Base
# Модели для ORM
from .auth.models import User
# Роутеры для API
from .auth.views import router as auth_router
from .monitoring.views import router as monitoring_router

router = APIRouter()

router.include_router(monitoring_router, prefix='/monitoring', tags=['Мониторинг'])
router.include_router(auth_router, prefix='/auth', tags=['Аутентификация'])

__all__ = [
    'router',
    'Base'
]
