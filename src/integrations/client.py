from http import HTTPStatus, HTTPMethod
from typing import Mapping, Any

from httpx import AsyncClient
from httpx import TimeoutException, NetworkError, ProtocolError, HTTPStatusError

from core.config import config
from core.exceptions import NotAvailableServerException, BadRequestException, ForbiddenException, CustomException


class ApiClient:
    """
    Клиент API-сервиса
    """

    def __init__(self, base_url: str, headers: Mapping[str, str] = None):
        self.base_url = base_url.rstrip('/')
        self.additional_headers = headers

    def __make_url(self, path: str):
        """
        Возвращает нормализованный url
        """
        path = path.lstrip("/")
        return "/".join((self.base_url, path))

    async def _make_request(
        self,
        method: HTTPMethod,
        path: str,
        params: Mapping[str, str] = None,
        data: Mapping[str, Any] | None = None,
        json: Any | None = None,
        headers: Mapping[str, str] | None = None,
        timeout: int = config.INTREGRATION_DEFAULT_TIMEOUT
    ):
        """
        Выполняет запрос к внешнему API-сервису
        """
        request_headers = self.additional_headers or {}
        if headers:
            request_headers.update(headers)
        async with AsyncClient() as client:
            try:
                response = await client.request(
                    method=method.value,
                    url=self.__make_url(path),
                    data=data,
                    json=json,
                    params=params,
                    headers=request_headers,
                    timeout=timeout
                )
                return response
            except (TimeoutException, NetworkError, ProtocolError) as e:
                raise NotAvailableServerException() from e
            except HTTPStatusError as e:
                match e.response.status_code:
                    case HTTPStatus.BAD_REQUEST:
                        raise BadRequestException(message=e.response.json()) from e
                    case HTTPStatus.UNAUTHORIZED, HTTPStatus.FORBIDDEN:
                        raise ForbiddenException() from e
                    case _:
                        raise CustomException(message=e.response.json()) from e

    async def get(self, path: str, params: Mapping[str, str] | None = None):
        """
        Выполняет GET-запрос
        """
        return await self._make_request(method=HTTPMethod.GET, path=path, params=params)

    async def post(self, path: str, data: Mapping[str, Any] | None = None, json: Any | None = None):
        """
        Выполняет POST-запрос
        """
        return await self._make_request(method=HTTPMethod.POST, path=path, data=data, json=json)

    async def put(self, path: str, data: Mapping[str, Any] | None = None, json: Any | None = None):
        """
        Выполняет PUT-запрос
        """
        return await self._make_request(method=HTTPMethod.PUT, path=path, data=data, json=json)

    async def patch(self, path: str, data: Mapping[str, Any] | None = None, json: Any | None = None):
        """
        Выполняет PATCH-запрос
        """
        return await self._make_request(method=HTTPMethod.PATCH, path=path, data=data, json=json)

    async def delete(self, path: str, data: Mapping[str, Any] | None = None, json: Any | None = None):
        """
        Выполняет DELETE-запрос
        """
        return await self._make_request(method=HTTPMethod.DELETE, path=path, data=data, json=json)
